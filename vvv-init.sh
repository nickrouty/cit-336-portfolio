echo "Creating database 'school-cit-336-portfolio' (if it does not exist)..."

mysql -u root --password=root -e "CREATE DATABASE IF NOT EXISTS \`school-cit-336-portfolio\`"
mysql -u root --password=root -e "GRANT ALL PRIVILEGES ON \`school-cit-336-portfolio\`.* TO wp@localhost IDENTIFIED BY 'wp';"

